#! /usr/bin/env python3

import os
import sys
import glob
import shutil
import subprocess

POT_FILE = 'po/text.pot'
POT_TMP_DIR = 'pot'

if __name__ == '__main__':

    if not shutil.which('po4a-gettextize'):
        print("Error: Could not find 'po4a-gettextize' executable, "
              "please make sure po4a is installed.")
        sys.exit(1)

    if not shutil.which('msgcat'):
        print("Error: Could not find 'msgcat' executable, "
              "please make sure gettext is installed.")
        sys.exit(1)

    tmp_pot_files = []
    for md_file in glob.glob('**/*.md', recursive=True):
        print('processing', md_file, '...', end='')
        subdir, filename = os.path.split(md_file)
        potname = filename[:-2] + 'pot'
        targetdir = os.path.join(POT_TMP_DIR, subdir)
        targetpath = os.path.join(targetdir, potname)
        os.makedirs(targetdir, exist_ok=True)
        cmd = [shutil.which('po4a-gettextize'),
               '-f', 'text',
               '-o', 'markdown',
               '-L', 'utf-8',
               '-M', 'utf-8',
               '-m', md_file,
               '-p', targetpath]
        tmp_pot_files.append(targetpath)
        try:
            subprocess.check_call(cmd, shell=False)
            print(' OK')
        except Exception as e:
            print(' failed!')
            print(e)
            print("Error: calling '{}' failed.".format(' '.join(cmd)))
            sys.exit(2)

    print('assembling', POT_FILE, '...', end="")
    cmd = [shutil.which('msgcat'),
           '--no-wrap',
           '--lang=en_US',
           '--add-location=file',
           '-o', POT_FILE]
    cmd.extend(tmp_pot_files)
    subprocess.check_call(cmd, shell=False)
    shutil.rmtree(POT_TMP_DIR)
    print(' OK')
