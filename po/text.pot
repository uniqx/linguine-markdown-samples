# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-03-21 17:04+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title #
#: README.md
#, no-wrap
msgid "Linguine Markdown Samples"
msgstr ""

#. type: Plain text
#: README.md
msgid "This repo contains a couple of markdown files. They serve as a sample project which is supposed to get translated."
msgstr ""

#. type: Plain text
#: README.md
msgid "also a link: [this project](https://gitlab.com/uniqx/linguine-markdown-sample)"
msgstr ""

#. type: Title #
#: index.md
#, no-wrap
msgid "Index"
msgstr ""

#. type: Plain text
#: index.md
msgid "Just a boring file devoid of all meaning. This simply serves as material for testing markdown translation tools."
msgstr ""

#. type: Plain text
#: index.md
msgid "New text."
msgstr ""

#. type: Title ##
#: index.md
#, no-wrap
msgid "those special cases"
msgstr ""

#. type: Title ###
#: index.md
#, no-wrap
msgid "code listings"
msgstr ""

#. type: Plain text
#: index.md
msgid "Also lets add some __code listings__ just so see how that goes:"
msgstr ""

#. type: Plain text
#: index.md
msgid "``` $ make me a sandwitch no Dave ```"
msgstr ""

#. type: Title ####
#: index.md
#, no-wrap
msgid "Well?"
msgstr ""

#. type: Plain text
#: index.md
msgid "``` $ sudo make me a sandwitch yes Dave ```"
msgstr ""

#. type: Title ###
#: index.md
#, no-wrap
msgid "Quotes"
msgstr ""

#. type: Plain text
#: index.md
#, no-wrap
msgid ""
"> Let me put it this way, Mr. Amor. The 9000 series is the most reliable\n"
"> computer ever made. No 9000 computer has ever made a mistake or distorted\n"
"> information. We are all, by any practical definition of the words, foolproof\n"
"> and incapable of error.\n"
msgstr ""
